<div class="gambar"  style="background-color:black">
<h1 style="text-align:center; color:white">Welcome to K7-KOST</h1>
<div class="container">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="<?php echo base_url(); ?>/img/kamar.jpg" style="height:400px;" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="<?php echo base_url(); ?>/img/bathroom.jpg" style="height:400px;" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="<?php echo base_url(); ?>/img/gambar.jpg" style="height:400px;" class="d-block w-100" alt="...">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
  <br>
</div>
</div>


<div   style="height:310px" class="page-footer text-light fixed-bottom bg-success">
<div class="row" style="margin-top:30px">

  <div class="col-sm-1">

  </div>
  <div class="col-sm-4">
    <ul style="list-style-type:circle;">
      <li>Nama Kost : K7 Kost</li>
      <li>Alamat : JL.Geger Arum No.156 RT.07 RW.01 Gegerkalong Kec.Sukasari Kota Bandung Jawa Barat</li>
      <li>Kontak : 0853 1606 1605</li>
    </ul>
  </div>
  <div class="col-sm-3" >
    <ul style="list-style-type:circle;">
      <h4>Fasilitas :</h4>
      <ul style="margin-left:75px">
        <li>Kamar (Luas 4x3)</li>
        <li>Kamar Mandi di Dalam</li>
        <li>Free Listrik</li>
        <li>Free Wifi</li>
        <li>Dapur Umum</li>
        <li>Tempat Parkir</li>
        <li>Satpam 24 Jam</li>
        <li>Tempat Jemuran</li>
      </ul>
    </ul>
  </div>

  <div class="col-sm-3">
    <ul style="list-style-type:circle;">
      <h4>Isi Kamar :</h4>
      <ul  style="margin-left:90px">
        <li>Kasur 1x</li>
        <li>Meja Belajar 1x</li>
        <li>Kursi 1x</li>
        <li>Lemari Baju 1x</li>
        <li>Rak Sepatu 1x</li>
        <li>Rak Piring 1x</li>
        <li>Karpet 2x</li>
      </ul>
    </ul>
  </div>
</div>

</div>

  <script src="<?php echo base_url(); ?>js/jquery-3.3.1.slim.min.js"></script>
  <script src="<?php echo base_url(); ?>js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

</body>
</html>
